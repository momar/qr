all: main.go
	rm -rf build
	mkdir -p build
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags '-s -w -extldflags "-static"' -o build/qr .
	CGO_ENABLED=0 GOOS=linux GOARCH=arm go build -a -ldflags '-s -w -extldflags "-static"' -o build/qr.arm .
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -a -ldflags '-s -w -extldflags "-static"' -o build/qr.exe .
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -a -ldflags '-s -w -extldflags "-static"' -o build/qr.darwin .
	upx build/qr build/qr.arm build/qr.exe build/qr.darwin
	gzip build/qr build/qr.arm build/qr.darwin
	zip build/qr.exe.zip build/qr.exe && rm build/qr.exe
