package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/skip2/go-qrcode"
	"github.com/muesli/termenv"
)

func main() {
	var f = flag.NewFlagSet("qr", flag.ContinueOnError)
	f.Usage = func() {}

	var levelStr = f.String("level", "low", "error recovery level: low|medium|high|highest")
	var nocolor = f.Bool("no-color", false, "disable colors; inverts QR codes on dark terminals; not compatible with -ansi")
	var ansi = f.Bool("ansi", false, "use ANSI color background instead of Unicode characters; will make the QR code 4x as large")
	help := f.Parse(os.Args[1:])

	var text = f.Arg(0)
	var errStr = ""

	var level qrcode.RecoveryLevel
	switch *levelStr {
	case "low": level = qrcode.Low
	case "medium": level = qrcode.Medium
	case "high": level = qrcode.High
	case "highest": level = qrcode.Highest
	default: errStr = "invalid value for -level"
	}

	var format Formatter
	switch *ansi {
	case false: format = UnicodeFormatter
	case true: format = ANSIFormatter
	default: errStr = "invalid value for -format"
	}
	if *nocolor && *ansi {
		errStr = "-no-color is not compatible -ansi"
	}

	if args := f.Args(); len(args) != 1 || text == "" {
		errStr = "exactly one content argument is required"
	}

	if text == "" || help != nil || errStr != "" {
		fmt.Printf(termenv.String("qr - generate QR codes in your unicode terminal\n").Bold().String())
		fmt.Printf("Usage: qr [-level ...] [-no-color|-ansi] <content>\n\n")
		if errStr != "" {
			fmt.Printf(termenv.String("Error: %s").Foreground(termenv.ANSIColor(1)).Bold().String() + "\n\n", errStr)
		}
		fmt.Printf("Set the content to \"-\" to read from stdin.\n")
		fmt.Printf("Options:\n")
		f.PrintDefaults()
		os.Exit(1)
	}

	if text == "-" {
		data, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			fmt.Printf(termenv.String("stdin read error: %s").Foreground(termenv.ANSIColor(1)).Bold().String() + "\n", err)
			os.Exit(1)
		}
		text = strings.TrimSuffix(string(data), "\n")
	}

	qr, err := qrcode.New(text, level)
	if err != nil {
		fmt.Printf(termenv.String("qrcode error: %s").Foreground(termenv.ANSIColor(1)).Bold().String() + "\n", err)
		return
	}

	output := bytes.NewBuffer([]byte{})
	bitmap := qr.Bitmap()
	format(bitmap, output, !*nocolor)
	output.WriteTo(os.Stdout)
}

type Formatter func(bitmap [][]bool, output *bytes.Buffer, color bool)

func UnicodeFormatter(bitmap [][]bool, output *bytes.Buffer, color bool) {
	height := len(bitmap)
	width := len(bitmap[0])
	var line *bytes.Buffer
	for y := 1; y < height - 1; y += 2 {
		line = bytes.NewBuffer([]byte{})
		for x := 1; x < width; x++ {
			if bitmap[y][x] && bitmap[y+1][x] {
				line.WriteRune('█')
			} else if bitmap[y][x] && !bitmap[y+1][x] {
				line.WriteRune('▀')
			} else if !bitmap[y][x] && bitmap[y+1][x] {
				line.WriteRune('▄')
			} else if !bitmap[y][x] && !bitmap[y+1][x] {
				line.WriteRune(' ')
			}
		}
		if color {
			output.WriteString(termenv.String(line.String()).Background(termenv.RGBColor("#ffffff")).Foreground(termenv.RGBColor("#000000")).String())
		} else {
			output.WriteString(line.String())
		}
		output.WriteByte('\n')
	}
}

func ANSIFormatter(bitmap [][]bool, output *bytes.Buffer, color bool) {
	height := len(bitmap)
	width := len(bitmap[0])
	for y := 1; y < height - 1; y ++ {
		for x := 1; x < width - 1; x++ {
			if bitmap[y][x] {
				output.WriteString(termenv.String("  ").Background(termenv.RGBColor("#000000")).String())
			} else {
				output.WriteString(termenv.String("  ").Background(termenv.RGBColor("#ffffff")).String())
			}
		}
		output.WriteByte('\n')
	}
}
