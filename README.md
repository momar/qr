# qr

This is a command-line tool to generate QR codes in your terminal.

## Installation

Just grab the correct binary from [the releases](/momar/qr/releases), unpack it & copy it to `C:\Windows` or `sudo install -m755 qr /usr/local/bin/`, and you're good to go!

## Usage

![](https://i.vgy.me/GZd8WX.png)
