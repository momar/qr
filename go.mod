module codeberg.org/momar/qr

go 1.13

require (
	github.com/muesli/termenv v0.4.0
	github.com/skip2/go-qrcode v0.0.0-20191027152451-9434209cb086
)
